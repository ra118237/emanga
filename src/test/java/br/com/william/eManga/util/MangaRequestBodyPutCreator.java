package br.com.william.eManga.util;

import br.com.william.eManga.request.manga.MangaRequestBodyPost;
import br.com.william.eManga.request.manga.MangaRequestBodyPut;

public class MangaRequestBodyPutCreator {
  public static MangaRequestBodyPut mangaRequestBodyPutCreator(){
      return MangaRequestBodyPut.builder()
              .id(1L)
              .name("YYu Yu Hakusho")
              .autor("Yoshihiro Togashi")
              .numeroPages(500)
              .build();
  }
}
