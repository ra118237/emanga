package br.com.william.eManga.util;

import br.com.william.eManga.request.manga.MangaRequestBodyPost;

public class MangaRequestBodyPostCreator {
  public static MangaRequestBodyPost mangaRequestBodyPostCreator(){
      return MangaRequestBodyPost.builder()
              .name("YYu Yu Hakusho")
              .autor("Yoshihiro Togashi")
              .numeroPages(500)
              .build();
  }
}
