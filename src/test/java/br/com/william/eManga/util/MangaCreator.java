package br.com.william.eManga.util;

import br.com.william.eManga.model.Manga;

import java.util.List;

public class MangaCreator {

  public static Manga getMangaValid(){
    return Manga.builder()
                    .id(1L)
                    .name("Dragon Ball Z")
                    .autor("Akira toriyama")
                    .numeroPages(1000)
                    .build();
  }

  public static List<Manga> listOfMagaForToSave() {
    return List.of(
            Manga.builder()
                    .name("Dragon Ball Z")
                    .autor("Akira toriyama")
                    .numeroPages(1000)
                    .build(),
            Manga.builder()
                    .name("Hunter vs Hunter")
                    .autor("Yoshihiro Togashi")
                    .numeroPages(500)
                    .build(),
            Manga.builder()
                    .name("YYu Yu Hakusho")
                    .autor("Yoshihiro Togashi")
                    .numeroPages(500)
                    .build()
    );
  }
  public static List<Manga> listOfMaga(){
    return List.of(
             Manga.builder()
                     .id(1L)
                     .name("Dragon Ball Z")
                     .autor("Akira toriyama")
                     .numeroPages(1000)
                     .build(),
            Manga.builder()
                    .id(2L)
                    .name("Hunter vs Hunter")
                    .autor("Yoshihiro Togashi")
                    .numeroPages(500)
                    .build(),
            Manga.builder()
                    .id(3L)
                    .name("YYu Yu Hakusho")
                    .autor("Yoshihiro Togashi")
                    .numeroPages(500)
                    .build()
    );
  }

  public static Manga mangaToBySave(){
    return  Manga.builder()
            .name("Dragon Ball Z")
            .autor("Akira toriyama")
            .numeroPages(1000)
            .build();
  }

  public static Manga manga(){
    return  Manga.builder()
            .id(1L)
            .name("Dragon Ball Z")
            .autor("Akira toriyama")
            .numeroPages(1000)
            .build();
  }
}
