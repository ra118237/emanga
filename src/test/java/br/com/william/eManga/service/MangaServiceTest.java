package br.com.william.eManga.service;

import br.com.william.eManga.exceptions.BadRequestException;
import br.com.william.eManga.model.Manga;
import br.com.william.eManga.repository.MangaRepository;
import br.com.william.eManga.util.MangaCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;


@ExtendWith(SpringExtension.class)
@DisplayName("Test Manga Service Test")
class MangaServiceTest {
  @InjectMocks
  private MangaService mangaService;

  @Mock
  private MangaRepository mangaRepository;

  @BeforeEach
  public void setUp(){
    List<Manga> mangas = MangaCreator.listOfMaga();
    Page<Manga> pageManga = new PageImpl<>(mangas);

    BDDMockito.when(mangaRepository.findAll(ArgumentMatchers.any(PageRequest.class)))
            .thenReturn(pageManga);
    BDDMockito.when(mangaRepository.findAll())
            .thenReturn(mangas);
    BDDMockito.when(mangaRepository.findById(ArgumentMatchers.anyLong()))
            .thenReturn(Optional.of(MangaCreator.getMangaValid()));
    BDDMockito.when(mangaRepository.save(ArgumentMatchers.any(Manga.class)))
            .thenReturn(MangaCreator.getMangaValid());
    BDDMockito.doNothing().when(mangaRepository)
            .delete(ArgumentMatchers.any(Manga.class));
  }

  @Test
  @DisplayName("find by id or throw exception  manga when id not found")
  void findByIdOrThrowException_ThrowException_When_IdNotFound() {
    BDDMockito.when(mangaRepository.findById(ArgumentMatchers.anyLong()))
            .thenReturn(Optional.empty());

    Assertions.assertThatExceptionOfType(BadRequestException.class)
            .isThrownBy(() -> mangaService.findByIdOrThrowException(1L))
            .withMessageContaining("Manga nao encontrado");
  }

  @Test
  @DisplayName("save manga when sucessful")
  void save_Evento_When_Sucessful() {
    Manga mangaToBySave = MangaCreator.getMangaValid();
    mangaToBySave.setId(null);

    Manga mangaSave = mangaService.save(mangaToBySave);

    Assertions.assertThat(mangaSave)
            .isNotNull();
    Assertions.assertThat(mangaSave.getId())
            .isNotNull();
    Assertions.assertThat(mangaSave.getAutor())
            .isEqualTo(mangaToBySave.getAutor());
    Assertions.assertThat(mangaSave.getNumeroPages())
            .isEqualTo(mangaToBySave.getNumeroPages());
  }

  @Test
  @DisplayName("save manga when name is null")
  void save_Manga_When_NameIsNull() {
    BDDMockito.when(mangaRepository.save(ArgumentMatchers.any(Manga.class)))
            .thenThrow(ConstraintViolationException.class);

    Manga mangaToBySave = MangaCreator.mangaToBySave();

    Assertions.assertThatExceptionOfType(ConstraintViolationException.class)
            .isThrownBy(() -> mangaRepository.save(mangaToBySave));
  }

  @Test
  @DisplayName("Replace Manga when successful")
  public void save_ReplaceManga_When_Successful() {
    Manga mangaToBySave = MangaCreator.getMangaValid();
    mangaToBySave.setId(null);

    Manga manga = mangaService.save(mangaToBySave);
    Manga mangaReplaced = mangaService.save(manga);

    Assertions.assertThat(mangaReplaced)
            .isNotNull();
    Assertions.assertThat(mangaReplaced.getId())
            .isEqualTo(manga.getId());
    Assertions.assertThat(mangaReplaced.getName())
            .isEqualTo(manga.getName());
    Assertions.assertThat(mangaReplaced.getAutor())
            .isEqualTo(manga.getAutor());
    Assertions.assertThat(mangaReplaced.getNumeroPages())
            .isEqualTo(manga.getNumeroPages());
  }

  @Test
  @DisplayName("Delete Manga when successful")
  public void delete_Void_When_Successful() {
    BDDMockito.when(mangaRepository.findById(ArgumentMatchers.anyLong()))
            .thenReturn(Optional.empty());

    Manga mangaToBySave = MangaCreator.mangaToBySave();
    Manga manga = mangaService.save(mangaToBySave);

    Assertions.assertThatExceptionOfType(BadRequestException.class)
            .isThrownBy(() ->  mangaService.remove(manga.getId()))
            .withMessageContaining("Manga nao encontrado");
  }

  @Test
  @DisplayName("Delete Manga when id not found")
  public void delete_Void_When_EventoNotFound() {
    BDDMockito.when(mangaRepository.findById(ArgumentMatchers.anyLong()))
            .thenReturn(Optional.empty());

    Manga mangaNotPresentInDatabase = MangaCreator.mangaToBySave();
    mangaNotPresentInDatabase.setId(0L);

    Assertions.assertThatExceptionOfType(BadRequestException.class)
            .isThrownBy(() -> mangaService.remove(mangaNotPresentInDatabase.getId()))
            .withMessageContaining("Manga nao encontrado");
  }

}