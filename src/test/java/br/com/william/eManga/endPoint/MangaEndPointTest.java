package br.com.william.eManga.endPoint;

import br.com.william.eManga.exceptions.BadRequestException;
import br.com.william.eManga.model.Manga;
import br.com.william.eManga.request.manga.MangaRequestBodyPost;
import br.com.william.eManga.request.manga.MangaRequestBodyPut;
import br.com.william.eManga.service.MangaService;
import br.com.william.eManga.util.MangaCreator;
import br.com.william.eManga.util.MangaRequestBodyPostCreator;
import br.com.william.eManga.util.MangaRequestBodyPutCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintViolationException;
import java.util.List;

@ExtendWith(SpringExtension.class)
@DisplayName("Test MangaEndPoint")
class MangaEndPointTest {
  @InjectMocks
  private MangaEndPoint mangaEndPoint;

  @Mock
  private MangaService mangaService;

  @BeforeEach
  public void setUp(){
    List<Manga> mangas = MangaCreator.listOfMaga();
    Page<Manga> pageManga = new PageImpl<>(mangas);

    BDDMockito.when(mangaService.findAllPageable(ArgumentMatchers.any(PageRequest.class)))
            .thenReturn(pageManga);
    BDDMockito.when(mangaService.findByIdOrThrowException(ArgumentMatchers.anyLong()))
            .thenReturn(MangaCreator.getMangaValid());
    BDDMockito.when(mangaService.save(ArgumentMatchers.any(Manga.class)))
            .thenReturn(MangaCreator.getMangaValid());
    BDDMockito.when(mangaService.save(ArgumentMatchers.any(Manga.class)))
            .thenReturn(MangaCreator.getMangaValid());
    BDDMockito.doNothing().when(mangaService)
            .remove(ArgumentMatchers.anyLong());
  }

  @Test
  @DisplayName("find by id  manga when id not found")
  void findById_ThrowException_When_IdNotFound() {
    BDDMockito.when(mangaService.findByIdOrThrowException(ArgumentMatchers.anyLong()))
            .thenThrow(BadRequestException.class);

    Assertions.assertThatExceptionOfType(BadRequestException.class)
            .isThrownBy(() -> mangaEndPoint.findById(1L));
  }

  @Test
  @DisplayName("save manga when sucessful")
  void save_Evento_When_Sucessful() {
    MangaRequestBodyPost mangaToBySave = MangaRequestBodyPostCreator.mangaRequestBodyPostCreator();

    ResponseEntity<Manga> mangaSaveResponse = mangaEndPoint.save(mangaToBySave);
    Manga mangaSave = mangaSaveResponse.getBody();

    Assertions.assertThat(mangaSaveResponse)
            .isNotNull();
    Assertions.assertThat(mangaSaveResponse.getStatusCode())
            .isNotNull()
            .isEqualTo(HttpStatus.CREATED);
    Assertions.assertThat(mangaSave)
            .isNotNull()
            .isEqualTo(MangaCreator.getMangaValid());
  }

//  @Test
//  @DisplayName("save manga when name is null")
//  void save_Manga_When_NameIsNull() {
//    BDDMockito.when(mangaService.save(ArgumentMatchers.any(Manga.class)))
//            .thenThrow(ConstraintViolationException.class);
//
//    MangaRequestBodyPost mangaToBySave = MangaRequestBodyPostCreator.mangaRequestBodyPostCreator();
//    mangaToBySave.setName(null);
//
//    ResponseEntity<Manga> mangaSaveRespnse = mangaEndPoint.save(mangaToBySave);
//
//    Assertions.assertThat(mangaSaveRespnse.getStatusCode())
//            .isNotNull()
//            .isEqualTo(HttpStatus.BAD_REQUEST);
//    Assertions.assertThat(mangaSaveRespnse.getBody())
//            .isNull();
//  }

  @Test
  @DisplayName("Replace Manga when successful")
  public void save_ReplaceManga_When_Successful() {
    MangaRequestBodyPut mangaToBySave = MangaRequestBodyPutCreator.mangaRequestBodyPutCreator();
    mangaToBySave.setId(null);

    ResponseEntity<Void> replace = mangaEndPoint.replace(mangaToBySave);

    Assertions.assertThat(replace)
            .isNotNull();
    Assertions.assertThat(replace.getStatusCode())
            .isNotNull()
            .isEqualTo(HttpStatus.NO_CONTENT);
  }

  @Test
  @DisplayName("Delete Manga when successful")
  public void delete_Void_When_Successful() {
    ResponseEntity<Void> remove = mangaEndPoint.remove(1L);

    Assertions.assertThat(remove.getStatusCode())
            .isNotNull()
            .isEqualTo(HttpStatus.NO_CONTENT);
  }

  @Test
  @DisplayName("Delete Manga when id not found")
  public void delete_Void_When_EventoNotFound() {
    BDDMockito.when(mangaService.findByIdOrThrowException(ArgumentMatchers.anyLong()))
            .thenThrow(BadRequestException.class);

    Manga mangaNotPresentInDatabase = MangaCreator.mangaToBySave();
    mangaNotPresentInDatabase.setId(0L);

    ResponseEntity<Void> remove = mangaEndPoint.remove(mangaNotPresentInDatabase.getId());

    Assertions.assertThat(remove.getStatusCode())
            .isEqualTo(HttpStatus.NO_CONTENT);

    Assertions.assertThat(remove.getBody())
            .isNull();
  }
}