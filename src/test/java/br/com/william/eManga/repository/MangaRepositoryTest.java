package br.com.william.eManga.repository;

import br.com.william.eManga.model.Manga;
import br.com.william.eManga.util.MangaCreator;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@DataJpaTest
@ExtendWith(SpringExtension.class)
@DisplayName("test MangaRepository")
class MangaRepositoryTest {

  @Autowired
  private MangaRepository mangaRepository;

  @Test
  @DisplayName("find All the mangas when sucessful")
  void findAll_ListOfManga_When_Sucessful() {
    List<Manga> mangas = MangaCreator.listOfMagaForToSave();
    mangaRepository.saveAll(mangas);

    List<Manga> allMangas = mangaRepository.findAll();

    Assertions.assertThat(allMangas)
            .isNotNull()
            .hasSize(mangas.size())
            .isEqualTo(mangas);
    Assertions.assertThat(allMangas.stream()
              .map(Manga::getId)
              .collect(Collectors.toList()))
            .isNotNull()
            .doesNotContain(null, 0L);
  }

  @Test
  @DisplayName("find All the mangas when database is empty sucessful")
  void findAll_ListOfMangas_When_DatabaseIsEmptySucessful() {

    List<Manga> allMangas = mangaRepository.findAll();

    Assertions.assertThat(allMangas)
            .isNotNull()
            .hasSize(0);
  }

  @Test
  @DisplayName("find by id  manga when sucessful")
  void findById_OptinalManga_When_Sucessful() {
    Manga mangaToBySave = MangaCreator.mangaToBySave();

    Manga mangaSave = mangaRepository.save(mangaToBySave);

    Assertions.assertThat(mangaSave.getId())
            .isNotNull();
    Assertions.assertThat(mangaSave)
            .isNotNull();
  }

  @Test
  @DisplayName("find by id  manga when id not found")
  void findById_OptinalManga_When_IdNotFound() {
    Manga mangaToBySave = MangaCreator.mangaToBySave();
    Manga mangaSave = mangaRepository.save(mangaToBySave);

    Optional<Manga> mangaOptinal = mangaRepository.findById(mangaToBySave.getId() + 1L);

    Assertions.assertThat(mangaOptinal)
            .isEmpty();
  }

  @Test
  @DisplayName("save manga when sucessful")
  void save_Evento_When_Sucessful() {
    Manga mangaToBySave = MangaCreator.mangaToBySave();

    Manga mangaSave = mangaRepository.save(mangaToBySave);

    Assertions.assertThat(mangaSave)
            .isNotNull();
    Assertions.assertThat(mangaSave.getId())
            .isNotNull();
    Assertions.assertThat(mangaSave.getAutor())
            .isEqualTo(mangaToBySave.getAutor());
    Assertions.assertThat(mangaSave.getNumeroPages())
            .isEqualTo(mangaToBySave.getNumeroPages());
  }

  @Test
  @DisplayName("save manga when name is null")
  void save_Manga_When_NameIsNull() {
    Manga mangaToBySave = MangaCreator.mangaToBySave();
    mangaToBySave.setName(null);

    Assertions.assertThatExceptionOfType(ConstraintViolationException.class)
            .isThrownBy(() -> mangaRepository.save(mangaToBySave))
            .withMessageContaining("field name cannot null or empty");
  }

  @Test
  @DisplayName("save manga when field autor is null")
  void save_Manga_When_AutorIsNull() {
    Manga mangaToBySave = MangaCreator.mangaToBySave();
    mangaToBySave.setAutor(null);

    Assertions.assertThatExceptionOfType(ConstraintViolationException.class)
            .isThrownBy(() -> mangaRepository.save(mangaToBySave))
            .withMessageContaining("field autor cannot null");
  }

  @Test
  @DisplayName("Replace Manga when successful")
  public void save_ReplaceManga_When_Successful() {
    Manga mangaToBySave = MangaCreator.mangaToBySave();

    Manga manga = mangaRepository.save(mangaToBySave);
    mangaToBySave.setAutor("Some autor");
    Manga mangaReplaced = mangaRepository.save(manga);

    Assertions.assertThat(mangaReplaced)
            .isNotNull();
    Assertions.assertThat(mangaReplaced.getId())
            .isEqualTo(manga.getId());
    Assertions.assertThat(mangaReplaced.getName())
            .isEqualTo(manga.getName());
    Assertions.assertThat(mangaReplaced.getAutor())
            .isEqualTo(manga.getAutor());
    Assertions.assertThat(mangaReplaced.getNumeroPages())
            .isEqualTo(manga.getNumeroPages());
  }

  @Test
  @DisplayName("Delete Manga when successful")
  public void delete_Void_When_Successful() {
    Manga mangaToBySave = MangaCreator.mangaToBySave();
    Manga manga = mangaRepository.save(mangaToBySave);

    mangaRepository.delete(manga);

    Optional<Manga> mangaOptionalFromDatabase = mangaRepository.findById(manga.getId());

    Assertions.assertThat(mangaOptionalFromDatabase)
            .isNotNull()
            .isEmpty();
  }

  @Test
  @DisplayName("Delete Manga when id not found")
  public void delete_Void_When_EventoNotFound() {
    Manga mangaToBySave = MangaCreator.mangaToBySave();
    Manga manga = mangaRepository.save(mangaToBySave);
    manga.setId(0L);

    mangaRepository.delete(manga);

    Optional<Manga> mangaOptionalFromDatabase = mangaRepository.findById(manga.getId());

    Assertions.assertThat(mangaOptionalFromDatabase)
            .isEmpty();
  }
}