package br.com.william.eManga.mapper.manga;

import br.com.william.eManga.model.Manga;
import br.com.william.eManga.request.manga.MangaRequestBodyPost;
import br.com.william.eManga.request.manga.MangaRequestBodyPut;
import br.com.william.eManga.util.MangaRequestBodyPostCreator;
import br.com.william.eManga.util.MangaRequestBodyPutCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Manga Mapper test")
class MangaMapperTest {

  @Test
  @DisplayName("MangaRequestBodyPost to Manga when sucessfull")
  public void mangaRequestBodyPostToManga_Manga_When_Sucessfull() {
    MangaRequestBodyPost mangaRequestBodyPost = MangaRequestBodyPostCreator.mangaRequestBodyPostCreator();

    Manga manga = MangaMapper.INSTANCE.mangaRequestBodyPostToManga(mangaRequestBodyPost);

    Assertions.assertThat(manga)
                    .isNotNull();
    Assertions.assertThat(manga.getId())
            .isNull();
    Assertions.assertThat(manga.getName())
            .isNotNull()
            .isEqualTo(mangaRequestBodyPost.getName());
    Assertions.assertThat(manga.getAutor())
            .isNotNull()
            .isEqualTo(mangaRequestBodyPost.getAutor());
    Assertions.assertThat(manga.getNumeroPages())
            .isNotNull()
            .isEqualTo(mangaRequestBodyPost.getNumeroPages());
  }

  @Test
  @DisplayName("MangaRequestBodyPut to Manga when sucessfull")
  public void mangaRequestBodyPutToManga_Manga_When_Sucessfull() {
    MangaRequestBodyPut mangaRequestBodyPut = MangaRequestBodyPutCreator.mangaRequestBodyPutCreator();

    Manga manga = MangaMapper.INSTANCE.mangaRequestBodyPutToManga(mangaRequestBodyPut);

    Assertions.assertThat(manga)
            .isNotNull();
    Assertions.assertThat(manga.getId())
            .isEqualTo(mangaRequestBodyPut.getId());
    Assertions.assertThat(manga.getName())
            .isNotNull()
            .isEqualTo(mangaRequestBodyPut.getName());
    Assertions.assertThat(manga.getAutor())
            .isNotNull()
            .isEqualTo(mangaRequestBodyPut.getAutor());
    Assertions.assertThat(manga.getNumeroPages())
            .isNotNull()
            .isEqualTo(mangaRequestBodyPut.getNumeroPages());
  }
}