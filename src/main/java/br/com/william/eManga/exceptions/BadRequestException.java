package br.com.william.eManga.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;

@Component
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {
  public BadRequestException(){
    super();
  }

  public BadRequestException(String mensage){
    super(mensage);
  }

  public BadRequestException(Exception ex){
    super(ex);
  }
}
