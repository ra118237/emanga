package br.com.william.eManga.endPoint;

import br.com.william.eManga.mapper.manga.MangaMapper;
import br.com.william.eManga.model.Manga;
import br.com.william.eManga.request.manga.MangaRequestBodyPost;
import br.com.william.eManga.request.manga.MangaRequestBodyPut;
import br.com.william.eManga.service.MangaService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/mangas")
@AllArgsConstructor
@Log4j2
public class MangaEndPoint {
  private MangaService mangaService;

  @GetMapping
  public ResponseEntity<Page<Manga>> findPageable(Pageable pageable){
    return ResponseEntity.ok(mangaService.findAllPageable(pageable));
  }

  @GetMapping("/findbyid/{id}")
  public ResponseEntity<Manga> findById(@PathVariable Long id) {
    Manga manga = mangaService.findByIdOrThrowException(id);
    return ResponseEntity.ok(manga);
  }

  @PostMapping
  public ResponseEntity<Manga> save(@RequestBody @Valid MangaRequestBodyPost mangaRequestBodyPost) {
    log.info(mangaRequestBodyPost);
    Manga manga = MangaMapper.INSTANCE.mangaRequestBodyPostToManga(mangaRequestBodyPost);
    log.info(manga);
    return new ResponseEntity<>(mangaService.save(manga), HttpStatus.CREATED);
  }

  @PutMapping
  public ResponseEntity<Void> replace(@RequestBody @Valid MangaRequestBodyPut mangaRequestBodyPut) {
    Manga manga = MangaMapper.INSTANCE.mangaRequestBodyPutToManga(mangaRequestBodyPut);
    mangaService.save(manga);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> remove(@PathVariable Long id) {
    mangaService.remove(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
