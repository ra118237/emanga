package br.com.william.eManga.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import br.com.william.eManga.model.Manga;

public interface MangaRepository extends JpaRepository<Manga, Long> {

}
