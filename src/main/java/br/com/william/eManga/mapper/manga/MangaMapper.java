package br.com.william.eManga.mapper.manga;

import br.com.william.eManga.model.Manga;
import br.com.william.eManga.request.manga.MangaRequestBodyPost;
import br.com.william.eManga.request.manga.MangaRequestBodyPut;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MangaMapper {
  MangaMapper INSTANCE = Mappers.getMapper(MangaMapper.class);

  Manga mangaRequestBodyPostToManga(MangaRequestBodyPost mangaRequestBodyPost);
  Manga mangaRequestBodyPutToManga(MangaRequestBodyPut mangaRequestBodyPut);

}
