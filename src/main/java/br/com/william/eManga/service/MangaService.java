package br.com.william.eManga.service;

import br.com.william.eManga.exceptions.BadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.william.eManga.model.Manga;
import br.com.william.eManga.repository.MangaRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
@AllArgsConstructor
public class MangaService {
	private MangaRepository mangaRepository;
	
	public Page<Manga> findAllPageable(Pageable pageable){
		return mangaRepository.findAll(pageable);
	}
	
	public Manga findByIdOrThrowException(Long id) {
		return mangaRepository.findById(id)
				.orElseThrow(()->new BadRequestException("Manga nao encontrado"));
	}

	public Manga save(Manga manga) {
		return mangaRepository.save(manga);
	}

	public void remove(Long id) {
		Manga manga = findByIdOrThrowException(id);
		mangaRepository.delete(manga);
	}
}
