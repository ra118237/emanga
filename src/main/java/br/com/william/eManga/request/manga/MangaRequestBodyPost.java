package br.com.william.eManga.request.manga;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Data
@Builder
public class MangaRequestBodyPost {

  @NotEmpty(message = "The manga name cannot be empty")
  @NotNull(message = "The manga name cannot be null")
  private String name;

  @NotEmpty(message = "The field autor cannot be empty")
  @NotNull(message = "The field autor cannot be null")
  private String autor;

  @NotNull(message = "The field numeroPages cannot be null")
  private Integer numeroPages;
}
