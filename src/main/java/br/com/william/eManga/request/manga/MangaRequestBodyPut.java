package br.com.william.eManga.request.manga;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@Data
@Builder
public class MangaRequestBodyPut {

  @NotEmpty(message = "The id from manga cannot be empty")
  @NotNull(message = "The id from manga cannot be null")
  private Long id;

  @NotEmpty(message = "The manga name cannot be empty")
  @NotNull(message = "The manga name cannot be null")
  private String name;

  @NotEmpty(message = "The field autor name cannot be empty")
  @NotNull(message = "The field autor name cannot be null")
  private String autor;

  @NotEmpty(message = "The field numeroPages name cannot be empty")
  @NotNull(message = "The field numeroPages name cannot be null")
  private Integer numeroPages;
}
